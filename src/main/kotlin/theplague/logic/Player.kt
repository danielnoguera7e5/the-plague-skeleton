package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position

class Player(var currentPosition: Position): IPlayer, Iconizable {

    fun equip(item: Item) {
        when(item){
            is Weapon -> currentWeapon = item
            is Vehicle -> currentVehicle = item
        }

    }

    fun useVehicle() {
        if (currentVehicle.use()) {
            currentVehicle.timesLeft -= 1
        } else {
            currentVehicle = OnFoot(0)
        }
    }

    fun canMoveTo(position: Position): Boolean {
        return currentVehicle.canMove(currentPosition,position)
    }

    override val icon: String
        get() = "\uD83D\uDEB6"

    override var turns: Int = 0
    override var livesLeft: Int = 3

    /**
     * The player current weapon
     */
    override var currentWeapon: Weapon = Hand(0)

    /**
     * The player current vehicle
     */
    override var currentVehicle: Vehicle = OnFoot(0)

}