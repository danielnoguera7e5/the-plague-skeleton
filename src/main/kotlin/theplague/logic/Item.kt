package theplague.logic

import theplague.interfaces.Iconizable

abstract class Item(var timesLeft: Int): Iconizable {

    fun use() : Boolean {
        if (timesLeft == 1) {
            return false
        }
        return true
    }

}