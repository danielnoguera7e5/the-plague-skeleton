package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import javax.swing.Icon


class Territory: ITerritory {

    var player: Player? = null
    var item: Item? = null
    var colony: Colony? = null

    override fun iconList(): List<Iconizable> {
        val list = mutableListOf<Iconizable>()

        if (player != null) {
            list.add(player!!)
        }
        if (item != null) {
            list.add(item!!)
        }
        if (colony != null) {
            repeat(colony!!.size+1){
                list.add(colony!!) // no aconsegueixo retornar una llista de colonys per a que es pugui veure correctament al programa
            }
        }
        return list

    }


    fun exterminate(weapon: Weapon) {
        colony?.attacked(weapon)
        if (colony?.size == 0) {
            colony = null
        }
    }
}
