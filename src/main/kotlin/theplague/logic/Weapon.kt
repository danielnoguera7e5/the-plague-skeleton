package theplague.logic

abstract class Weapon(timesLeft: Int): Item(timesLeft) {

}

class Hand(timesLeft: Int): Weapon(timesLeft) {
    override val icon: String
        get() = "\uD83D\uDC46"
}
class Broom(timesLeft: Int): Weapon(timesLeft) {
    override val icon: String
        get() = "\uD83E\uDDF9"
}
class Sword(timesLeft: Int): Weapon(timesLeft) {
    override val icon: String
        get() = "\uD83D\uDDE1"
}